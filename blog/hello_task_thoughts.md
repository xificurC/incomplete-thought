# What is a task?

> A task is a value representing an action to be performed. [...] If threads were cheap and available everywhere, we could have represented tasks as zero-argument functions (aka thunks). Instead, we chose a purely asynchronous representation, providing efficiency and reach. You can think of tasks as asynchronous thunks, if you will.

There was a [reddit discussion](https://old.reddit.com/r/Clojure/comments/k2db8k/leonoelmissionary_a_functional_effect_and/ge0at66/) where the author further compares a task to JS promises:

> A promise is an eagerly memoized view of a task. [...] So a promise is a function that takes a task, runs that task, stores its result when it's completed, and returns a task completing with this memoized result as soon as it's available. [...] The price we pay to get this behavior is giving up on cancellation.

We got some hints here why was missionary built. A task is a concurrency primitive. It is not built on threads because they aren't cheap (JVM) or even available everywhere (JS). It is not built from futures/promises because they don't allow cancellation propagation.

To get a feeling let's try implementing some of missionary's primitives through clojure's futures.

## Tasks through futures

`sp` creates a sequential process. If we rely on running in futures we can just model this as a thunk:

```clj
(defmacro sp [& code] `(fn [] ~@code))
```

A task is run through `?`:

```clj
(defn ? [task] (task))
```

Sleeping is done through `sleep`:

```clj
(defn sleep [ms] (Thread/sleep ms))
```

The `slowmo-hello-world` works in Java, although the sleeps block the current thread:

```clj
(def nap (m/sleep 1000))
(def slowmo-hello-world
  (m/sp (println "Hello")
        (m/? nap)
        (println "World")
        (m/? nap)
        (println "!")))
```

Now the first interesting part we run into is implementing `join`:

```clj
(def chatty-hello-world
  (m/join vector slowmo-hello-world slowmo-hello-world))
```

This should first of all run the 2 tasks in parallel.

```clj
(defn join [joiner & tasks]
  (sp (apply joiner (map deref (mapv #(future (? %)) tasks)))))
```

This isn't a complete reimplementation, we're missing cancellation propagation

```clj
(def unreliable-hello-world
  (m/sp (println "Hello")
        (m/? (m/sleep 500))
        (throw (ex-info "Something went wrong." {}))))
```

With `unreliable-hello-world` in the mix we need to cancel running tasks. So we need to keep looking at the futures and if one throws stop all the rest:

```clj
(defn join [joiner & tasks]
  (sp
    (loop [done [] running (mapv #(future (? %)) tasks)]
      (if (seq running)
        (let [[done running] (reduce (fn [[d r] nx] 
                                       (let [v (try (deref nx 0 ::nope)
                                                    (catch Throwable t (run! future-cancel running) (throw t)))]
                                         (if (identical? ::nope v) [d (conj r nx)] [(conj d v) r])))
                                     [done []] running)]
          (recur done running))
        (apply joiner done)))))
```
